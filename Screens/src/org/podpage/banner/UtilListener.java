package org.podpage.banner;

import java.util.ArrayList;

import io.netty.channel.Channel;
import net.minecraft.server.v1_8_R2.EntityPlayer;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class UtilListener implements Listener {

	public ArrayList<Player> players = new ArrayList<Player>();

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		addChannelforPlayer(e.getPlayer());
		Main.sendMap(e.getPlayer());
		players.add(e.getPlayer());
	}

	@EventHandler
	public void onChangeWorld(PlayerChangedWorldEvent e) {
		if (players.contains(e.getPlayer())) {
			Main.sendMap(e.getPlayer());
		}
	}

	@EventHandler
	public void onRespawn(PlayerRespawnEvent e) {
		if (players.contains(e.getPlayer())) {
			Main.sendMap(e.getPlayer());
		}
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		if (players.contains(e.getPlayer())) {
			players.remove(e.getPlayer());
		}
	}

	public static void addChannelforPlayer(Player p) {
		try {
			EntityPlayer ep = ((CraftPlayer) p).getHandle();
			Channel channel = (Channel) Main.mChannel.get(ep.playerConnection.networkManager);
			channel.pipeline().addBefore("packet_handler", "Map", new MyChannelDuplexHandler());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@EventHandler
	public void onInventory(InventoryClickEvent e) {
		Inventory i = e.getInventory();
		if (i.getName().contains("�5BannerMenu")) {
			e.setCancelled(true);
			if (e.getCurrentItem() != null && e.getCurrentItem().getType().equals(Material.MAP)) {
				ItemStack item = e.getCurrentItem();
				if (item.hasItemMeta()) {
					if (item.getItemMeta().hasDisplayName()) {
						String name = item.getItemMeta().getDisplayName();
						if (name.startsWith("�5")) {
							e.getWhoClicked().closeInventory();
							Player p = (Player) e.getWhoClicked();
							p.sendMessage("�7Overview for Banner \"" + name + "�7\"");
							CommandManager.sendEditBar(p, name.substring(2));
						}
					}
				}
			}
		}
	}
}
