package org.podpage.banner;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.server.v1_8_R2.IChatBaseComponent;
import net.minecraft.server.v1_8_R2.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R2.PacketPlayOutChat;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.PluginCommand;
import org.bukkit.craftbukkit.v1_8_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {

	public static ArrayList<Integer> ignoredmap = new ArrayList<>();
	public static Main plugin;
	public static MapImage img;
	public static Field mChannel;

	static {
		System.setProperty("java.awt.headless", "true");
	}

	public static Main getInstance() {
		return plugin;
	}

	public void onLoad() {
		plugin = this;
	}

	@Override
	public void onEnable() {
		try {
			mChannel = net.minecraft.server.v1_8_R2.NetworkManager.class.getDeclaredField("k");
			mChannel.setAccessible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Bukkit.getPluginManager().registerEvents(new UtilListener(), this);
		PluginCommand c = getCommand("banner");
		c.setExecutor(new CommandManager());
		c.setTabCompleter(new CommandManager());
		FileManager.loadMapData();
		for (Player p : Bukkit.getOnlinePlayers()) {
			UtilListener.addChannelforPlayer(p);
			sendMap(p);
		}
	}

	@Override
	public void onDisable() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.kickPlayer("Reload!");
		}
	}

	public static void sendMap() {
		for (MapImage mi : MapImage.maplist.values()) {
			mi.sendMap();
		}
	}

	public static void sendMap(Player p) {
		for (MapImage mi : MapImage.maplist.values()) {
			mi.sendMap(p);
		}
	}

	public static void sendJson(Player p, String s) {
		IChatBaseComponent cs = ChatSerializer.a(s);
		PacketPlayOutChat packet = new PacketPlayOutChat(cs);
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
	}

	public static ItemStack createMap(int count, String name, List<String> lore) {
		ItemStack i = new ItemStack(Material.MAP, count);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		im.setLore(lore);
		i.setItemMeta(im);
		return i;
	}
}
