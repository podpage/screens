package org.podpage.banner;

import java.lang.reflect.Field;

public class Reflection {
	public static void setValue(Object packet, String s, Object fieldcontent) {
		try {
			Field field = packet.getClass().getDeclaredField(s);
			field.setAccessible(true);
			field.set(packet, fieldcontent);
			field.setAccessible(false);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static Object getValue(Object packet, String s) {
		try {
			Field field = packet.getClass().getDeclaredField(s);
			field.setAccessible(true);
			return field.get(packet);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
}