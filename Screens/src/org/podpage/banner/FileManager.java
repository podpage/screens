package org.podpage.banner;

import org.bukkit.configuration.file.FileConfiguration;

public class FileManager {
	public static void loadMapData() {
		FileConfiguration fc = Main.plugin.getConfig();
		fc.options().header("You can add \"ignored\" Maps by adding them below. Split the with a whitespace!");
		fc.options().copyDefaults(true);
		fc.addDefault("ignoredmap", "no ignored Maps!");
		Main.plugin.saveConfig();
		String link;
		Integer start;
		Integer width;
		if (fc.contains("map")) {
			for (String name : fc.getConfigurationSection("map").getKeys(false)) {
				link = fc.getString("map." + name + ".url");
				start = Integer.valueOf(fc.getInt("map." + name + ".start"));
				width = Integer.valueOf(fc.getInt("map." + name + ".width"));
				Integer height = Integer.valueOf(fc.getInt("map." + name + ".height"));
				boolean b = fc.getBoolean("map." + name + ".inuse");
				if (link.isEmpty())
					new MapImage(name, start.intValue(), width.intValue(), height.intValue()).use = b;
				else {
					new MapImage(link, name, start.intValue(), width.intValue(), height.intValue()).use = b;
				}
			}
		}
		if ((fc.contains("ignoredmap")) && (!fc.getString("ignoredmap").contains("Maps"))) {
			String[] ss = fc.getString("ignoredmap").split(" ");
			for (String s : ss) {
				if ((s != null) && (!s.isEmpty()))
					Main.ignoredmap.add(Integer.valueOf(Integer.parseInt(s)));
			}
		}
	}

	public static void addMapData(MapImage mi) {
		FileConfiguration fc = Main.plugin.getConfig();
		fc.set("map." + mi.name + ".url", mi.link);
		fc.set("map." + mi.name + ".start", Integer.valueOf(mi.start));
		fc.set("map." + mi.name + ".width", Integer.valueOf(mi.width));
		fc.set("map." + mi.name + ".height", Integer.valueOf(mi.height));
		fc.set("map." + mi.name + ".inuse", Boolean.valueOf(mi.use));
		Main.plugin.saveConfig();
	}

	public static void removeMapData(MapImage mi) {
		FileConfiguration fc = Main.plugin.getConfig();
		fc.set("map." + mi.name + ".url", null);
		fc.set("map." + mi.name + ".start", null);
		fc.set("map." + mi.name + ".width", null);
		fc.set("map." + mi.name + ".height", null);
		fc.set("map." + mi.name + ".inuse", null);
		fc.set("map." + mi.name, null);
		Main.plugin.saveConfig();
	}
}