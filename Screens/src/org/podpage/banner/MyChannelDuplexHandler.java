package org.podpage.banner;

import net.minecraft.server.v1_8_R2.PacketPlayOutMap;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;

public class MyChannelDuplexHandler extends ChannelDuplexHandler {

	@Override
	public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
		if (msg instanceof PacketPlayOutMap) {
			int i = (int) Reflection.getValue(msg, "a");
			if (i >= 10000) {
				Reflection.setValue(msg, "a", i - 10000);
			} else {
				return;
			}
		}
		super.write(ctx, msg, promise);

	}
}