package org.podpage.banner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class CommandManager implements TabCompleter, CommandExecutor {

	private static List<String> commandlist = Arrays.asList("activate", "add", "deactivate", "delete", "edit", "info", "list", "reload");

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if (label.equalsIgnoreCase("banner")) {
			if (sender instanceof Player) {
				if (args.length == 1) {
					String a = args[0];
					List<String> list = new ArrayList<>();
					for (String s : commandlist) {
						if (s.startsWith(a)) {
							list.add(s);
						}
					}
					return list;
				}
			}
		}
		return null;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (label.equalsIgnoreCase("banner")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage("§cYou must be a Player!");
				return false;
			}
			if (!sender.isOp()) {
				sender.sendMessage("§cYou don't have Permission to do that!");
				return false;
			}
			Player p = (Player) sender;
			if (args.length == 1 && args[0].equalsIgnoreCase("list")) {
				final Inventory inv;
				Integer size = MapImage.maplist.size();
				if (size > 0 && size < 54) {
					inv = Bukkit.createInventory(null, 9 * (Math.round((size + 8) / 9)), "§5BannerMenu");
					int i = 0;
					for (final MapImage mi : MapImage.maplist.values()) {
						i++;
						final int c = i;
						Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
							@Override
							public void run() {
								List<String> list = new ArrayList<>();
								list.add("§7Link: " + mi.link);
								list.add("§7Start: " + mi.start);
								list.add("§7Width: " + mi.width);
								list.add("§7Height: " + mi.height);
								if (mi.use) {
									list.add("§7InUse: §aYes");
								} else {
									list.add("§7InUse: §cNo");
								}
								inv.addItem(Main.createMap(mi.height * mi.width, "§5" + mi.name, list));
							}
						}, 5 + 1 * c);
					}
				} else if (MapImage.maplist.size() > 54) {
					inv = Bukkit.createInventory(null, 45, "§5BannerMenu§7 - 1");
				} else {
					p.sendMessage("§5Banner §7» No Banner set yet!");
					return true;
				}
				p.openInventory(inv);
			} else if (args.length == 1 && args[0].equalsIgnoreCase("reload")) {
				MapImage.maplist.clear();
				Main.ignoredmap.clear();
				FileManager.loadMapData();
				for (Player t : Bukkit.getOnlinePlayers()) {
					Main.sendMap(t);
				}
				p.sendMessage("");
				p.sendMessage("§7Loading §5" + MapImage.maplist.size() + " §7Banner and §5" + Main.ignoredmap.size()
						+ " §7ignored Maps from the Config");
			} else if (args.length == 2 && args[0].equalsIgnoreCase("add")) {
				String bannername = args[1];
				if (MapImage.maplist.containsKey(bannername)) {
					p.sendMessage("§7The Banner \"§5" + bannername + "§7\" already exists");
				} else {
					p.sendMessage("");
					p.sendMessage("§7Added new Banner \"§5" + bannername);
					MapImage im = new MapImage(bannername);
					sendEditBar(p, bannername);
					im.save();
				}
			} else if (args.length == 2 && args[0].equalsIgnoreCase("info")) {
				String bannername = args[1];
				if (MapImage.maplist.containsKey(bannername)) {
					MapImage mi = MapImage.maplist.get(bannername);
					p.sendMessage("");
					p.sendMessage("§7========= §5BannerInfo §7=========");
					p.sendMessage("§7|| Name: " + mi.name);
					p.sendMessage("§7|| Link: " + mi.link);
					p.sendMessage("§7|| Start: " + mi.start);
					p.sendMessage("§7|| Width: " + mi.width);
					p.sendMessage("§7|| Height: " + mi.height);
					if (mi.use) {
						p.sendMessage("§7|| InUse: §aYes");
					} else {
						p.sendMessage("§7|| InUse: §cNo");
					}

					p.sendMessage("§7=============================");
				} else {
					p.sendMessage("§7The Banner \"§5" + bannername + "§7\" does not exist!");
				}
			} else if (args.length == 2 && args[0].equalsIgnoreCase("edit")) {
				p.sendMessage("");
				String bannername = args[1];
				sendEditTest(p, bannername);
			} else if (args.length == 2 && args[0].equalsIgnoreCase("use")) {
				p.sendMessage("");
				String bannername = args[1];
				if (MapImage.maplist.containsKey(bannername)) {
					MapImage mi = MapImage.maplist.get(bannername);
					if (mi.img != null && !mi.link.isEmpty() && mi.width > 0 && mi.height > 0) {
						mi.use = !mi.use;
						mi.save();
						if (mi.use) {
							for (Player t : Bukkit.getOnlinePlayers()) {
								mi.sendMap(t);
							}
						}
					} else {
						p.sendMessage("§7The Banner \"§5" + bannername + "§7\" is not complete!");
					}
					sendEditTest(p, bannername);
				} else {
					p.sendMessage("§7The Banner \"§5" + bannername + "§7\" does not exist!");
				}
			} else if (args.length == 3 && args[0].equalsIgnoreCase("link")) {
				p.sendMessage("");
				String bannername = args[1];
				if (MapImage.maplist.containsKey(bannername)) {
					MapImage mi = MapImage.maplist.get(bannername);
					mi.setURL(args[2]);
					mi.save();
					sendEditTest(p, bannername);
				} else {
					p.sendMessage("§7The Banner \"§5" + bannername + "§7\" does not exist!");
				}
			} else if (args.length == 3 && args[0].equalsIgnoreCase("start")) {
				p.sendMessage("");
				String bannername = args[1];
				if (MapImage.maplist.containsKey(bannername)) {
					MapImage mi = MapImage.maplist.get(bannername);
					int i = 0;
					try {
						i = Integer.valueOf(args[2]);
					} catch (NumberFormatException nFE) {
						p.sendMessage("§7Error: §5" + args[2] + "§7 is no Integer!");
					}
					mi.start = i;
					sendEditTest(p, bannername);
					for (Player t : Bukkit.getOnlinePlayers()) {
						mi.sendMap(t);
					}
					mi.save();
				} else {
					p.sendMessage("§7The Banner \"§5" + bannername + "§7\" does not exist!");
				}
			} else if (args.length == 3 && args[0].equalsIgnoreCase("width")) {
				p.sendMessage("");
				String bannername = args[1];
				if (MapImage.maplist.containsKey(bannername)) {
					MapImage mi = MapImage.maplist.get(bannername);
					int i = 0;
					try {
						i = Integer.valueOf(args[2]);
					} catch (NumberFormatException nFE) {
						p.sendMessage("§7Error: §5" + args[2] + "§7 is no Integer!");
					}
					mi.width = i;
					sendEditTest(p, bannername);
					for (Player t : Bukkit.getOnlinePlayers()) {
						mi.sendMap(t);
					}
					mi.save();
				} else {
					p.sendMessage("§7The Banner \"§5" + bannername + "§7\" does not exist!");
				}
			} else if (args.length == 3 && args[0].equalsIgnoreCase("height")) {
				p.sendMessage("");
				String bannername = args[1];
				if (MapImage.maplist.containsKey(bannername)) {
					MapImage mi = MapImage.maplist.get(bannername);
					int i = 0;
					try {
						i = Integer.valueOf(args[2]);
					} catch (NumberFormatException nFE) {
						p.sendMessage("§7Error: §5" + args[2] + "§7 is no Integer!");
					}
					mi.height = i;
					sendEditTest(p, bannername);
					for (Player t : Bukkit.getOnlinePlayers()) {
						mi.sendMap(t);
					}
					mi.save();
				} else {
					p.sendMessage("§7The Banner \"§5" + bannername + "§7\" does not exist!");
				}
			} else if (args.length == 3 && args[0].equalsIgnoreCase("name")) {
				p.sendMessage("");
				String bannername = args[1];
				if (MapImage.maplist.containsKey(bannername)) {
					MapImage mi = MapImage.maplist.get(bannername);
					MapImage im = new MapImage(mi.link, args[2], mi.start, mi.width, mi.height);
					im.use = mi.use;
					MapImage.maplist.remove(bannername);
					FileManager.removeMapData(mi);
					im.save();
					sendEditTest(p, args[2]);
				} else {
					p.sendMessage("§7The Banner \"§5" + args[2] + "§7\" does not exist!");
				}
			} else if (args.length == 2 && args[0].equalsIgnoreCase("delete")) {
				String bannername = args[1];
				if (MapImage.maplist.containsKey(bannername)) {
					p.sendMessage("");
					String text = "{\"text\":\"\",\"extra\":[{\"text\":\"Are you sure?\",\"color\":\"gray\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Are you sure you want to delete this banner?\"}},{\"text\":\" | \",\"color\":\"gray\"},{\"text\":\"Yes\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/banner delete "
							+ bannername
							+ " yes\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Delete banner "
							+ bannername
							+ "\"}},{\"text\":\" | \",\"color\":\"gray\"},{\"text\":\"No\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/banner delete "
							+ bannername
							+ " no\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Don't delete banner "
							+ bannername
							+ "\"}},{\"text\":\" | \",\"color\":\"gray\"}]}";
					Main.sendJson(p, text);
				} else {
					p.sendMessage("");
					p.sendMessage("§7This banner does not exist!");
				}
			} else if (args.length == 2 && args[0].equalsIgnoreCase("activate")) {
				String bannername = args[1];
				if (MapImage.maplist.containsKey(bannername)) {
					MapImage mi = MapImage.maplist.get(bannername);
					if (mi.use) {
						p.sendMessage("§7The Banner \"§5" + bannername + "§7\" is already activated!");
					} else {
						if (mi.img != null && !mi.link.isEmpty() && mi.width > 0 && mi.height > 0) {
							p.sendMessage("§7The Banner \"§5" + bannername + "§7\" is now activated!");
							mi.use = true;
							mi.save();
							for (Player t : Bukkit.getOnlinePlayers()) {
								mi.sendMap(t);
							}
						} else {
							p.sendMessage("§7The Banner \"§5" + bannername + "§7\" is not complete!");
						}
					}
				}
			} else if (args.length == 2 && args[0].equalsIgnoreCase("deactivate")) {
				String bannername = args[1];
				if (MapImage.maplist.containsKey(bannername)) {
					MapImage mi = MapImage.maplist.get(bannername);
					if (mi.use) {
						mi.use = false;
						mi.save();
						p.sendMessage("§7The Banner \"§5" + bannername + "§7\" is now deactivated");
					} else {
						p.sendMessage("§7The Banner \"§5" + bannername + "§7\" is already deactivated!");
					}
				} else {
					p.sendMessage("§7The Banner \"§5" + bannername + "§7\" does not exist!");
				}
			} else if (args.length == 3 && args[0].equalsIgnoreCase("delete")) {
				p.sendMessage("");
				String bannername = args[1];
				if (args[2].equalsIgnoreCase("yes")) {
					if (MapImage.maplist.containsKey(bannername)) {
						MapImage mi = MapImage.maplist.get(bannername);
						FileManager.removeMapData(mi);
						MapImage.maplist.remove(bannername);
						p.sendMessage("§7The Banner \"§5" + bannername + "§7\" was deleted");
					} else {
						p.sendMessage("§7The Banner \"§5" + bannername + "§7\" does not exist!");
					}
				} else if (args[2].equalsIgnoreCase("no")) {
					p.sendMessage("§7The Banner \"§5" + bannername + "§7\" was not deleted!");
				}
			} else if (args.length == 0) {
				p.sendMessage("§7=========== §5Banner §7===========");
				for (String s : commandlist) {
					p.sendMessage("§7|| /banner " + s);
				}
				p.sendMessage("§7=============================");
			}
		}
		return true;
	}

	public static void sendEditBar(Player p, String s) {
		if (MapImage.maplist.containsKey(s)) {
			MapImage mi = MapImage.maplist.get(s);
			String text = null;
			if (mi.use) {
				text = "{\"text\":\"\",\"extra\":[{\"text\":\"⏩ \",\"color\":\"gray\",\"bold\":\"true\"},{\"text\":\" | \",\"color\":\"gray\"},{\"text\":\"Info\",\"color\":\"blue\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/banner info "
						+ s
						+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Show Info\"}},{\"text\":\" | \",\"color\":\"gray\"},{\"text\":\"Edit\",\"color\":\"dark_green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/banner edit "
						+ s
						+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Edit "
						+ s
						+ "\"}},{\"text\":\" | \",\"color\":\"gray\"},{\"text\":\"Delete\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/banner delete "
						+ s
						+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Delete "
						+ s
						+ "\"}},{\"text\":\" | \",\"color\":\"gray\"},{\"text\":\"Deactivate\",\"color\":\"gold\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/banner deactivate "
						+ s
						+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Deactivate "
						+ s
						+ "\"}},{\"text\":\" | \",\"color\":\"gray\"}]}";
			} else {
				text = "{\"text\":\"\",\"extra\":[{\"text\":\"”—\",\"color\":\"gray\",\"bold\":\"true\"},{\"text\":\" | \",\"color\":\"gray\"},{\"text\":\"Info\",\"color\":\"blue\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/banner info "
						+ s
						+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Show Info\"}},{\"text\":\" | \",\"color\":\"gray\"},{\"text\":\"Edit\",\"color\":\"dark_green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/banner edit "
						+ s
						+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Edit "
						+ s
						+ "\"}},{\"text\":\" | \",\"color\":\"gray\"},{\"text\":\"Delete\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/banner delete "
						+ s
						+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Delete "
						+ s
						+ "\"}},{\"text\":\" | \",\"color\":\"gray\"},{\"text\":\"Activate\",\"color\":\"gold\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/banner activate "
						+ s
						+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Activate "
						+ s
						+ "\"}},{\"text\":\" | \",\"color\":\"gray\"}]}";
			}
			Main.sendJson(p, text);
		}
	}

	public static void sendEditTest(Player p, String s) {
		if (MapImage.maplist.containsKey(s)) {
			MapImage mi = MapImage.maplist.get(s);
			p.sendMessage("");
			p.sendMessage("§7===== §5 Click to edit BannerInfo §7=====");
			String name = "{\"text\":\"|| Name: " + mi.name
					+ "\",\"color\":\"gray\",\"clickEvent\":{\"action\":\"suggest_command\",\"value\":\"/banner name " + mi.name + " " + mi.name
					+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Change banner name\"}}";
			Main.sendJson(p, name);
			String link = "{\"text\":\"|| Link: " + mi.link
					+ "\",\"color\":\"gray\",\"clickEvent\":{\"action\":\"suggest_command\",\"value\":\"/banner link " + mi.name + " " + mi.link
					+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Change banner link\"}}";
			Main.sendJson(p, link);

			String start = "{\"text\":\"|| Start: " + mi.start
					+ "\",\"color\":\"gray\",\"clickEvent\":{\"action\":\"suggest_command\",\"value\":\"/banner start " + mi.name + " " + mi.start
					+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Change banner start\"}}";
			Main.sendJson(p, start);
			String width = "{\"text\":\"|| Width: " + mi.width
					+ "\",\"color\":\"gray\",\"clickEvent\":{\"action\":\"suggest_command\",\"value\":\"/banner width " + mi.name + " " + mi.width
					+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Change banner width\"}}";
			Main.sendJson(p, width);
			String height = "{\"text\":\"|| Height: " + mi.height
					+ "\",\"color\":\"gray\",\"clickEvent\":{\"action\":\"suggest_command\",\"value\":\"/banner height " + mi.name + " " + mi.height
					+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Change banner height\"}}";
			Main.sendJson(p, height);
			if (mi.use) {
				String yes = "{\"text\":\"\",\"extra\":[{\"text\":\"|| InUse: \",\"color\":\"gray\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/banner use "
						+ s
						+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Change banner use\"}},{\"text\":\"Yes\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/banner use "
						+ s + "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Change banner use\"}}]}";
				Main.sendJson(p, yes);
			} else {
				String yes = "{\"text\":\"\",\"extra\":[{\"text\":\"|| InUse: \",\"color\":\"gray\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/banner use "
						+ s
						+ "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Change banner use\"}},{\"text\":\"No\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/banner use "
						+ s + "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"Change banner use\"}}]}";
				Main.sendJson(p, yes);
			}
			p.sendMessage("§7================================");
		} else {
			p.sendMessage("§7The Banner \"§5" + s + "§7\" does not exist!");
		}
	}
}
